mysql -u root

USE blog_db;

-- Creating table users
CREATE TABLE users (
email VARCHAR(50) NOT NULL,
password VARCHAR(20) NOT NULL,
datetime_created DATETIME NOT NULL
PRIMARY KEY (email)
);

INSERT INTO users (email, password, datetime_created)
VALUES('johnsmith@gmail.com', 'passwordA', '2021-01-01 01:00:00');

INSERT INTO users (email, password, datetime_created)
VALUES('juandelacruz@gmail.com', 'passwordB', '2021-01-01 02:00:00');

INSERT INTO users (email, password, datetime_created)
VALUES('janesmith@gmail.com', 'passwordC', '2021-01-01 03:00:00');

INSERT INTO users (email, password, datetime_created)
VALUES('mariadelacruz@gmail.com', 'passwordD', '2021-01-01 04:00:00');

INSERT INTO users (email, password, datetime_created)
VALUES('johndoe@gmail.com', 'passwordE', '2021-01-01 05:00:00');


-- Creating Table posts
CREATE TABLE posts (
user_id INT NOT NULL,
title VARCHAR(30) NOT NULL,
content VARCHAR(20) NOT NULL,
datetime_posted DATETIME NOT NULL
);

INSERT INTO posts (user_id, title, content, datetime_posted)
VALUES('1', 'First Code', 'Hello World!', '2021-01-02 01:00:00');


INSERT INTO posts (user_id, title, content, datetime_posted)
VALUES('1', 'Second Code', 'Hello Earth!', '2021-01-02 02:00:00');

INSERT INTO posts (user_id, title, content, datetime_posted)
VALUES('2', 'Third Code', 'Welcome to Mars!', '2021-01-02 03:00:00');

INSERT INTO posts (user_id, title, content, datetime_posted)
VALUES('4', 'Fourth Code', 'Bye bye solar system!', '2021-01-02 04:00:00');

-- Get all the post with an user_id of 1.
SELECT * FROM posts WHERE user_id = 1;

-- Get all the user's email and datetime of creation.
SELECT users.email, users.datetime_created FROM users;

-- Update a post's content to Hello to the people of the earth! where its initial content is Hello Earth! by using record's ID.
UPDATE posts SET content = "Hello to the people of the earth" WHERE user_id = 1 AND content = "Hello Earth!";

-- Delete user with an email of johndoe@gmail.com.
DELETE FROM users WHERE email = "johndoe@gmail.com";

